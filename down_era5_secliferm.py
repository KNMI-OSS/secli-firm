import cdsapi
import datetime    
import os
import gzip
import numpy as np
import sys
import pandas as pd
import xarray as xr

"""
Author: Folmer Krikken
Contact: folmer.krikken@climateradar.com
Date: 29-10-2021

Script to download SWH and surface wind speed of ERA5 from the climate data store, only for the North Sea. Currently, it downloads data ranging from 1981 to 2020. This data is publicly available

"""


bd = '/where/to/store/you/data/'

if not os.path.isdir(bd):
    os.system('mkdir -p '+bd)

c = cdsapi.Client()
date = datetime.date.today().isoformat()
dr = pd.date_range('1981-01-01','2020-01-01',freq='YS')

for dt in dr:    
    date = dt.strftime('%Y-%m-%d')
    print(date)
    for var_name,var in zip(['swh','wind'],[['significant_height_of_combined_wind_waves_and_swell'],['10m_u_component_of_wind', '10m_v_component_of_wind']]):
        filename = 'era5_secliferm_'+var_name+'_'+date+'.nc'
        print(bd+filename)
        if os.path.isfile(bd+filename):
            print(filename+' already exists..')
            continue
        else:
            c.retrieve(
                'reanalysis-era5-single-levels',
                {
                    'product_type': 'reanalysis',
                    'format': 'netcdf',
                    'variable' : var,
                    'year': dt.strftime('%Y'),
                    'month': [str(m).zfill(2) for m in range(1,13)],
                    'day': [
                        '01', '02', '03',
                        '04', '05', '06',
                        '07', '08', '09',
                        '10', '11', '12',
                        '13', '14', '15',
                        '16', '17', '18',
                        '19', '20', '21',
                        '22', '23', '24',
                        '25', '26', '27',
                        '28', '29', '30',
                        '31',
                    ],
                    'time': [
                        '00:00', '01:00', '02:00',
                        '03:00', '04:00', '05:00',
                        '06:00', '07:00', '08:00',
                        '09:00', '10:00', '11:00',
                        '12:00', '13:00', '14:00',
                        '15:00', '16:00', '17:00',
                        '18:00', '19:00', '20:00',
                        '21:00', '22:00', '23:00',
                    ],
                    'format': 'netcdf',
                    'area': [
                        61, -4, 51,
                        9,
                    ],                
                },
                bd+filename)


    
        