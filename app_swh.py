import plotly.express as px
import dash
from dash import dcc, html
import dash_bootstrap_components as dbc

#import geopandas as gp
import pandas as pd
import numpy as np
import json
import plotly.graph_objs as go
#import geojson
#from urllib.request import urlopen
import os
import pyproj
import xarray as xr
import plotly.tools as tls
import plotly.subplots as sbp
import glob
import pickle
import datetime
import copy
#from jupyter_dash import JupyterDash

"""
Author: Folmer Krikken
Contact: folmer.krikken@climateradar.com

This application uses Dash Plotly to disseminate seamless forecasts of significant wave height 
and windspeed for several windparks in the North Sea. It was developed in close collaboration with 
TenneT. It requires the data preprocessed by 'seclifirm_app_analyse.ipynb'. 
Currently, it is setup as a standalone application you can run locally.
"""


# Add these external stylesheets for nice loading phases
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css', 'https://codepen.io/chriddyp/pen/brPBPO.css']

 
bd = '/home/folmer/climexp_data/KPREPData/secliferm/'
if not os.path.isdir(bd):
    bd = '/home/oldenbor/climexp_data/KPREPData/secliferm/'
    
info = {'variables':{'Significant wave height':'swh','Surface windspeed':'wind'},
       }

timez_s5 = pd.to_datetime(xr.open_dataset(bd+'swh_s5_1981_2020_bc.nc').time[:].values)
timez_ext = pd.to_datetime(xr.open_dataset(bd+'swh_ext_2017_2020_bc.nc').time[:].values)
dict_times = {t.strftime('%Y-%m-%d'):pd.to_datetime(t) for t in timez_ext[::-1][:100]}

start_date = "2020-01-01"
end_date = datetime.datetime.now()


swh_thresholds= {'1 meter':1,'2 meter':2}

# Add some local CSS to style the figures
app_style = {'width': '95%', 'display': 'inline-block',
                                             'border-radius': '5px',
                                             'box-shadow': '2px 2px 2px grey',
                                             'background-color': '#DCDCDC',#f9f9f9',
                                             'padding': '10px',
                                             'margin-bottom': '10px',
                                             'margin-left': '10px',
                                             'textAlign':'center',
                    }

fig_style= {'width': '100%', 'display': 'inline-block',
                                             'border-radius': '5px',
                                             'box-shadow': '2px 2px 2px grey',
                                             'background-color': '#f9f9f9',
                                             'padding': '10px',
                                             'margin-bottom': '10px',
                                             'margin-left': '10px'}


# Add some external stylesheets
external_stylesheets = [dbc.themes.BOOTSTRAP,'https://codepen.io/chriddyp/pen/bWLwgP.css', 'https://codepen.io/chriddyp/pen/brPBPO.css'] 

#app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
#external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css', 'https://codepen.io/chriddyp/pen/brPBPO.css']

if __name__ == '__main__':
    app3 = dash.Dash(
    #app3 = JupyterDash(
        __name__, 
        #requests_pathname_prefix='/swh_dashboard/',  # comment out when running locally
        external_stylesheets=external_stylesheets,
        meta_tags=[{"name": "viewport", "content": "width=device-width"}],
        )
else:
    app3 = dash.Dash(
        __name__, 
        requests_pathname_prefix='/swh_dashboard/',  # comment out when running locally
        external_stylesheets=external_stylesheets,
        meta_tags=[{"name": "viewport", "content": "width=device-width"}],
        )   

server = app3.server

app3.config.suppress_callback_exceptions = True

###  Start making layout  ###

row1 =  html.Div(
            [
            dbc.Row(
                [
                dbc.Col(dbc.CardImg(src=app3.get_asset_url('knmi_logo.png')),width=2),
                dbc.Col([
                    dcc.Markdown('''
                    **This dashboard presents a seamless approach on combining forecasts of different length scales for significant wave height (SWH) and wind speed.**
                    * By selecting a windpark in the map (left figure) both figures on the right are updated for that specific location. 
                    * You can select the period of interest by changing the slider or by selecting a region with your mouse. 
                    * The forecasts are updated every Tuesday and Friday in the early morning.
                    * The SWH and wind speed represent the maximum hourly value over the available timestep. 
                    '''),
                    ],width=8),
                dbc.Col(dbc.CardImg(src=app3.get_asset_url('seclifirm_logo.png')),width=2),                    
                ],
                justify='center',style={'textAlign':'center'}),
            dbc.Row(
                [
                dbc.Col([
                    dcc.Markdown('''
                    ** NOTE: This dashboard is a trial climate service. Any feedback on this service can be send to _gertie.geertsema@knmi.nl_.** 
                    
                    '''),
                    ],width=9),
                ],
                justify='center',style={'textAlign':'center','color':'red'}),
            ])

row2 = html.Div(
            [
            dbc.Row(
                [
                dbc.Col([
                        html.Label('Select variable',title='Select the variable you are interested in',
                                  style={'text-decoration':'underline'}),
                        dcc.Dropdown(
                            id='predictand',
                            options=[{'label': i,'value': i} for i in info['variables'].keys()],
                            value=list(info['variables'].keys())[0],
                        ),
                ],width=2),
                dbc.Col([
                        html.Label('Select forecast time',title='Select the forecast time you are interested in',
                                  style={'text-decoration':'underline'}),
                        dcc.Dropdown(
                            id='fc_time',
                            options=[{'label': i,'value': i} for i in dict_times.keys()],
                            value=list(dict_times.keys())[0],
                        ),
                ],width=2),
                dbc.Col([
                    html.Label('SWH threshold [m]',title='Select the SWH threshold applicable to the ship of choice',
                                  style={'text-decoration':'underline'}),
                    dbc.Input(
                        id='swh_threshold', 
                        value=2.5, 
                        min=0.1, max=6., step=0.1,
                        type='number'),
                ],width=2),#, lg=3),
                dbc.Col([
                    html.Label('Wind threshold [m/s]',title='Select the wind speed threshold applicable',
                                  style={'text-decoration':'underline'}),
                    dbc.Input(
                        id='wind_threshold', 
                        value=15., 
                        min=0., max=40., step=1.,
                        type='number'),
                ],width=2),#, lg=3),                    
                dbc.Col([
                    html.Label('Acceptable risk [%]',title='Minimum likelyood that the selected variable stays below the selected threshold',
                                  style={'text-decoration':'underline'}),
                    dbc.Input(
                        id='chance_exceed', 
                        value=90., 
                        min=0, max=100, step=5,
                        type='number'),
                ],width=2),     
             ],justify='center'),
            ])

graphs = html.Div(
    [
    dbc.Row([
        dbc.Col([
            html.Div([
                html.P(
                    [
                        "Select a specific windpart / region, as marked on the ",
                        html.Span(
                            "map",
                            id="tooltip-target2",
                            style={"textDecoration": "underline", "cursor": "pointer"},
                        ),
                    ]
                ),
                dbc.Tooltip(
                    "If not all regions are visible, zoom out using the scrolling button over the map or by clicking and dragging the map",
                    target="tooltip-target2",
                    style = {'font-size': 12},
                ),                   
                dcc.Graph(id='windparks'),
                ]),
                ],
            #style={'width':'65vh','display': 'inline-block','margin': {'b': 10, 'r': 10, 'l': 10, 't': 10}}),
            #style=fig_style, width=12,md=4),
            style={'display': 'inline-block',
                                             'border-radius': '5px',
                                             'box-shadow': '2px 2px 2px grey',
                                             'background-color': '#f9f9f9',
                                             'padding': '10px',
                                             'margin-bottom': '10px',
                                             'margin-left': '10px'},
            width=12,md=4),
        dbc.Col([
            html.Div([
                #html.Label('Select the time period of interest by using the slider'),
                html.P(
                    [
                        "Select the time period of interest by using the slider. The ",
                        html.Span(
                            "time step differs",
                            id="tooltip-target",
                            style={"textDecoration": "underline", "cursor": "pointer"},
                        ),
                        " for the chosen value ",
                    ]
                ),
                dbc.Tooltip(
                    "For the 14 and 46 days forecasts the time step is hourly for the first 4 days, 3-hourly up to 6 days and then 6-hourly. "
                    "For the 7 and 12 months all data is transformed to 24-hourly data. "
                    "Note that all data represents the maximum hourly value over the timestep. E.g. a 24-hourly timestep shows the maximum 1-hourly value ",
                    target="tooltip-target",
                    style = {'font-size': 12}
                ),                
                dcc.RangeSlider(
                        id='sel_times',
                        min=0,
                        max=365,
                        step=None,
                        marks={
                            0: '',
                            14: '14 days',
                            46: '46 days',
                            210: '7 months',
                            365: '12months'
                        },
                        value=[0, 14],
                    )],
                style= fig_style),                
            html.Div([
                dcc.Graph(id='timeseries')],
                style= fig_style),
            html.Div([
                dcc.Graph(id='timeseries2')],
                style= fig_style),
            
                ],
        style={'display':'inline-block'},width=12,md=7), #'width':'65%',
        ]),#,justify='center'),
    ])

app3.layout = dbc.Container(children=[
                            html.Div([
                                html.Div([
                                   html.Br(),
                                   row1,
                                   html.Br(),
                                   row2,
                                   html.Br(),
                                   graphs,
                                ],style={'width': '95%', 'display': 'inline-block',
                                                 'border-radius': '5px',
                                                 'box-shadow': '2px 2px 2px grey',
                                                 'background-color': '#DCDCDC',#f9f9f9',
                                                 'padding': '10px',
                                                 'margin-bottom': '10px',
                                                 'margin-left': '10px',
                                                 'textAlign':'center',},
                                )],style={'textAlign':'center'}),#,'borderBottom':'darkgrey'}),

                            ],
                            fluid=True,
                            #style=app_style,

                            )



### Done making layout ###



### Start making plots ###

def create_timeseries(clickData,predictand,fc_time,swh_threshold,wind_threshold,chance_exceed,sel_times):
    print(' ')
    print('>>> Starting create_timeseries <<<')
    #print(sel_times)
    print(' ')
    #print(predictand)
    if clickData == None:
        clickData = {'points': [{'curveNumber': 0, 'pointNumber': 0, 'pointIndex': 0, 'location': 'HK-Z', 'z': 1}]}    
    
    wp = clickData['points'][0]['location']
    
    var = info['variables'][predictand]
    if var == 'swh':
        th = swh_threshold
        fig_title = 'ECMWF forecast: '+fc_time+' -- SWH -- Windpark: '+wp
    elif var == 'wind':
        th = wind_threshold
        fig_title = 'ECMWF forecast: '+fc_time+' -- Wind -- Windpark: '+wp
    
    #th = swh_threshold

    # Load last extended range forecast
    ext_wp = xr.open_dataset(bd+'swh_ext_2017_2020_bc.nc')[var].sel(time=fc_time,loc=wp).load()#.squeeze()
    tstart = pd.to_datetime(ext_wp.time[0].values)
    tend = tstart + pd.DateOffset(days=sel_times[1])
    time_ext = pd.to_datetime(ext_wp.time.values+ext_wp.step.values)
    ext_wp = ext_wp.squeeze().drop('time').rename({'step':'time'}).assign_coords(time=time_ext)
    #print('ext_wp',ext_wp)
    
    
    # Load CRPSS of extended range and rename some of the dimensions
    crpss_ext = xr.open_dataset(bd+'swh_ext_crpss_refclim.nc')[[var,var+'_nc']].sel(loc=wp).isel(step=slice(None,len(time_ext)))
    crpss_ext = crpss_ext.rename({'step':'time'}).assign_coords(time=time_ext)

    time_fc = time_ext

    if sel_times[1] <= 46:
        fc = ext_wp
        crpss = crpss_ext

        

    if sel_times[1] > 46: 
        # Load seasonal forecast
        fc_time_s5 = fc_time[:7]+'-01' # Take the month of the fc_time and select the first day (start of S5 forecast)
        print('here!',fc_time_s5)
        try:
            s5_wp = xr.open_dataset(bd+'swh_s5_1981_2020_bc.nc').sel(time=fc_time_s5,loc=wp)[var].load()
        except KeyError: # Seasonal forecast not yet updated, use previous month
            fc_time_s5 = (pd.to_datetime(fc_time_s5) - pd.DateOffset(months=1)).strftime('%Y-%m-%d')
            s5_wp = xr.open_dataset(bd+'swh_s5_1981_2020_bc.nc').sel(time=fc_time_s5,loc=wp)[var].load()
        print('here!',fc_time_s5)

            
        time_s5_start = (time_ext[-1]+pd.DateOffset(days=1)).strftime('%Y-%m-%d') # Take the last day of ext range +1 for the start of S5 data
         
        
        
        time_s5 = pd.date_range(start=s5_wp.time.values,freq='D',periods=len(s5_wp.step)) # Create array of times for S5
        
        # Now select only the part of S5 that does not overlap with extended range forecasts
        s5_wp = s5_wp.drop('time').rename({'step':'time'}).assign_coords(time=time_s5).sel(time=slice(time_s5_start,None))
        
        # Combine ext and S5 to a single forecast for 'seamless forecasts' and construct quantiles
        fc = ext_wp.combine_first(s5_wp.sel(time=slice(time_s5_start,None)))
        time_fc = time_fc.union(pd.date_range(start=time_s5_start,end=time_s5[-1],freq='D'))
        
        # Load skill score and combine with extended range crpss
        crpss_s5 = xr.open_dataset(bd+'swh_s5_crpss_refclim.nc')[[var,var+'_nc']].sel(loc=wp)
        crpss_s5 = crpss_s5.rename({'step':'time'}).assign_coords(time=time_s5)
        crpss = crpss_ext.combine_first(crpss_s5.sel(time=slice(time_s5_start,None)))        
        
        
    
    if sel_times[1] > 300:
        # First create the right times needed for the climatology
        time_era5_start = time_s5[-1] + pd.DateOffset(days=1)
        time_era5_end = tstart - pd.DateOffset(days=1) + pd.DateOffset(years=1)
        times_era5 = pd.date_range(time_era5_start,time_era5_end,freq='D')
        # Load climatology
        era5_swh_doy = xr.open_dataset(bd+'era5_clim_'+var+'_doy.nc').sel(loc=wp,dayofyear=times_era5.dayofyear).load()
        #era5_swh_doy = era5_swh_doy.assign_coords(dayofyear=np.arange(len(era5_swh_doy.time)))#.rename({'time':'number'})
        subset = np.random.choice(500,51,replace=True)
        era5_swh_doy = era5_swh_doy.rename({'dayofyear':'time'}).assign_coords(time=times_era5)[var][:,subset]
        crpss_clim = xr.full_like(era5_swh_doy.isel(number=0).drop('number'),0)
        fc = fc.combine_first(era5_swh_doy.assign_coords(number=np.arange(51)))
        crpss = crpss.combine_first(crpss_clim)
        time_fc = time_fc.union(times_era5)
 
    
    
    
    #crpss = crpss[var+'_nc']

    # If seasonal forecasts or a full year is selected, then resample everything to daily data
    if sel_times[1] > 46:
        fc = fc.resample(time='D').max()
        time_fc = pd.to_datetime(fc.time.values)
        crpss = crpss.resample(time='D').mean()

        
    fc_qt = fc.quantile([0.05,0.5,0.95],dim='number')
    #Load ERA5 quantiles
    era5_wp_clim = xr.open_dataset(bd+'era5_clim_'+var+'_qt.nc').sel(loc=wp)[var].load()
    time_era = pd.date_range(start=tstart,end=tend,freq='D')

    # Select the specific days than are needed to plot and roll the data so that the fc_time becomes first in the dataset
    era5_wp_clim_ss = era5_wp_clim.sel(dayofyear=np.unique(time_fc.dayofyear))
    era5_wp_clim_ssroll = era5_wp_clim.roll(dayofyear=-time_fc.dayofyear[0],roll_coords=True)
    
    ### Start plotting data for Fig 1 ###
    
    modez = 'lines'
    shape = 'linear' # use hvh for stepline, linear for normal line
    fig = sbp.make_subplots(rows=2,cols=1,shared_xaxes=True,vertical_spacing=0.01,row_heights=[0.95,0.05])

    # Plot individual members in gray
    fig.add_trace(go.Scatter(x=time_fc,y=fc.isel(number=0).values.squeeze(),mode=modez,name='ind. members',line=dict(color='gray',shape=shape),legendgroup='ensemble',showlegend=True,opacity=0.2))
    for i in fc.number[1:]:
        fig.add_trace(go.Scatter(x=time_fc,y=fc.sel(number=i).values.squeeze(),mode=modez,name='ens. members',line=dict(color='gray',shape=shape),legendgroup='ensemble',showlegend=False,opacity=0.2))
    # Add forecast quantiles
    fig.add_trace(go.Scatter(x=time_fc,y=fc_qt.sel(quantile=0.5).values.squeeze(),mode=modez,name='Forecast [5, 50 and 95 %]',line=dict(color='blue',shape=shape),legendgroup='s5'))
    fig.add_trace(go.Scatter(x=time_fc,y=fc_qt.sel(quantile=0.05).values.squeeze(),mode=modez,name='S5 quantiles [0.05,0.50,0.95]',line=dict(color='blue',shape=shape),legendgroup='s5',showlegend=False))
    fig.add_trace(go.Scatter(x=time_fc,y=fc_qt.sel(quantile=0.95).values.squeeze(),mode=modez,name='S5 quantiles [0.05,0.50,0.95]',line=dict(color='blue',shape=shape),legendgroup='s5',showlegend=False))
    # Add climatology quantiles
    fig.add_trace(go.Scatter(x=time_era,y=era5_wp_clim_ssroll.sel(quantile=0.5).values.squeeze(),mode=modez,name='Climatology [5, 50 and 95%]',line=dict(color='black',shape=shape),legendgroup='era5'))
    fig.add_trace(go.Scatter(x=time_era,y=era5_wp_clim_ssroll.sel(quantile=0.05).values.squeeze(),mode=modez,name='Climatology [0.05,0.50,0.95]',line=dict(color='black',shape=shape),legendgroup='era5',showlegend=False))
    fig.add_trace(go.Scatter(x=time_era,y=era5_wp_clim_ssroll.sel(quantile=0.95).values.squeeze(),mode=modez,name='Climatology [0.05,0.50,0.95]',line=dict(color='black',shape=shape),legendgroup='era5',showlegend=False))    
    
    # Add swh thresholds
    fig.add_trace(go.Scatter(x=[tstart,tend],y=[th,th],name='Threshold',mode='lines',line=dict(color='red')))
    
    if var == 'swh':
        yaxis_title = 'Sign. wave height [m]'
    elif var == 'wind':
        yaxis_title = 'Wind speed (10m) [m/s]'
    
    fr = [ 0.0, 0.1,0.3,0.45,0.7,0.99,1.0]
    tv = [-0.2,-0.1,0.1,0.30,0.5,0.8]
    colorz = ['#000099','#3355ff','#66aaff','#77ffff','#808080','#ffff33','#ffaa00','#ff4400','#cc0022']
    colorz = ['#77ffff','#808080','#ffff33','#ffaa00','#ff4400','#cc0022']
    colorsceel=[ 
                 [fr[0], colorz[0]], [fr[1], colorz[0]],   
                 [fr[1], colorz[1]], [fr[2], colorz[1]],  
                 [fr[2], colorz[2]], [fr[3], colorz[2]], 
                 [fr[3], colorz[3]], [fr[4], colorz[3]], 
                 [fr[4], colorz[4]], [fr[5], colorz[4]],
                 [fr[5], colorz[5]], [fr[6], colorz[5]],
               ]     
    
    fig.add_trace(go.Heatmap(x=time_fc,
                             #y=['smoothed clim.','raw clim.'],
                             y=[''],
                             #z=crpss.to_array().values,
                             z=[crpss[var].values],
                             zmin=tv[0],
                             zmax=tv[-1],
                             #zsmooth = 'best',
                             colorscale=colorsceel,
                             #xgap=5,
                             #ygap=5,
                             colorbar=dict(
                                    x=1.05,
                                    y=0.2,
                                    lenmode='fraction', len=0.7, 
                                    title='How skillful are the forecasts?',
                                    #fontsize=12,
                                    titleside='top',
                                    #titlefont=dict(size=18),
                                    tickvals = [-0.15,0,0.2,0.4,0.65],
                                    ticks='outside',
                                    ticktext = ['worse than climatology','same as climatology','reasonable skill','good skill','very good skill'],
                                    #yanchor = 'top',
                                    ),
                             ),row=2,col=1),
    fig.update_layout(go.Layout(
            title = fig_title,
            title_x = 0.5,
            font=dict(size=14),
            margin = {'l': 50, 'b': 5, 'r': 10, 't': 35},
            yaxis=dict(title=yaxis_title),  
            ))

    if sel_times[1] == 210:
        tend = time_s5[-1]
    #elif sel_time[1] == 365:
    #    tend = 
    # Change xaxis range based on rangeslider output (sel_times)
    fig.update_xaxes(range=[tstart,tend], row=1, col=1)
    fig.update_xaxes(range=[tstart,tend], row=2, col=1)
    
    
    
    ### Start plotting data for Fig 2 ###
    
    if var == 'swh':
        th = swh_threshold
        fig_title = 'Non-exceedence probability for '+str(th)+' meter sign. wave height'
    elif var == 'wind':
        th = wind_threshold
        fig_title = 'Non-exceedence probability for '+str(th)+' m/s wind speed'    
    
    era5_swh_doyz = xr.open_dataset(bd+'era5_clim_'+var+'_doy.nc').sel(loc=wp).load()
    time_era = pd.date_range(start=tstart,freq='D',periods=len(era5_swh_doyz.dayofyear))    
    
    era5_prob = ( (era5_swh_doyz[var] > th).sum(dim='number') / len(era5_swh_doyz.number) ) * 100.

    swh_excp = 100.- ((fc > th).sum(dim='number') / (len(fc.number)-fc.isnull().sum(dim='number').values))*100.
    tmp_era5_prob = (100.- era5_prob).roll(dayofyear=-time_fc.dayofyear[0],roll_coords=True)

    # Start constructing figure
    modez = 'lines'
    shape = 'linear' # use hvh for stepline, linear for normal line
    fig2 = sbp.make_subplots(rows=2,cols=1,shared_xaxes=True,vertical_spacing=0.01,row_heights=[0.9,0.1])

    fig2.add_trace(
        go.Scatter(x=time_fc,y=swh_excp.values.squeeze(),mode=modez,name='Forecast (S5)',line=dict(color='blue')),row=1,col=1)
    fig2.add_trace(
        go.Scatter(x=time_era,y=tmp_era5_prob.values.squeeze(),mode=modez,line=dict(color='black'),name='Climatology (ERA5)                    '),row=1,col=1)
    
    fig2.add_trace(
        go.Scatter(x=[time_fc[0],time_era[-1]],y=[chance_exceed,chance_exceed],mode='lines',name='Acceptable risk',line=dict(color='red')))
    
    gonogo = np.full((len(swh_excp.values)),np.nan)
    
    # Overwrite part of gonogo with extended range and seas5 data
    gonogo[:len(swh_excp.values)][swh_excp.values < chance_exceed] = -0.9
    gonogo[:len(swh_excp.values)][swh_excp.values >= chance_exceed] = 0.9
  
    fr = [ 0.0, 0.1,0.3,0.45,0.7,0.99,1.0]
    tv = [-0.2,-0.1,0.1,0.30,0.5,0.8]
    colorz = ['#000099','#3355ff','#66aaff','#77ffff','#808080','#ffff33','#ffaa00','#ff4400','#cc0022']
    colorz = ['red','green','#ffff33','#ffaa00','#ff4400','#cc0022']
    colorsceel=[ 
                 [0, colorz[0]], [0.5, colorz[0]],
                 [0.5, colorz[1]], [1., colorz[1]],  
               ]         
    
    fig2.add_trace(go.Heatmap(x=time_fc,
                             y=['Go / Nogo'],
                             z=[gonogo],
                             zmin=-1,
                             zmax=1,
                             #zsmooth = 'best',
                             colorscale=colorsceel,
                             #xgap=1,
                             #ygap=5,
                             colorbar=dict(
                                    x=1.0,
                                    y=0.4,
                                    lenmode='fraction', len=0.5, 
                                    title='Go / No go?',
                                    #fontsize=12,
                                    titleside='top',
                                    #titlefont=dict(size=18),
                                    tickvals = [-0.5,0.5],
                                    ticks='outside',
                                    ticktext = ['no go','go'],
                                    #yanchor = 'top',
                                    ),
                             ),row=2,col=1),

    fig2.update_layout(go.Layout(
            title = fig_title,
            title_x = 0.5,
            #hovermode="x unified",
            font=dict(size=14),
            margin = {'l': 10, 'b': 60, 'r': 10, 't': 35},
            autosize=True,
            #width=800.,
            #height=350.,
            yaxis=dict(title='non-exceedence prob. [%]'),  
            xaxis=dict(range=[time_era[0],time_era[sel_times[1]]]), ## Assuming daily data..        
            )) 
    if sel_times[1] == 210:
        tend = time_s5[-1]
    #elif sel_time[1] == 365:
    #    tend = 
    # Change xaxis range based on rangeslider output (sel_times)
    fig2.update_xaxes(range=[tstart,tend], row=1, col=1)
    fig2.update_xaxes(range=[tstart,tend], row=2, col=1)
    
    return fig, fig2
    

def create_windpark(clickData,predictand,fc_time):
    #print(clickData)
    #with open(bd+'shp/overige_windenergie_studiegebieden.geojson') as geofile:
    #    gj = json.load(geofile)

    gj_file = bd+'shp/windparks_gj.pkl'
    if not os.path.isfile(gj_file):
        with open(bd+'shp/overige_windenergie_studiegebieden.geojson') as geofile:
            gj = json.load(geofile)
        
        for i,g in enumerate(gj['features']):    
            point = g['geometry']['coordinates'][0]
            transformer = pyproj.Transformer.from_crs('EPSG:23031', "epsg:4326")
            latlon = [[list(transformer.transform(*p)[::-1]) for p in point]]
            gj['features'][i]['geometry']['coordinates'] = latlon
            #print(g)
            print(g['properties']['WINDGEBIED']+':')
            print(np.array([np.mean(np.array(latlon)[0,:,0]),np.mean(np.array(latlon)[0,:,1])]))
            gj['features'][i]['id'] = gj['features'][i]['properties']['OBJECTID']
        # Add point on north sea
        gg = copy.deepcopy(g)
        gg['properties']['OBJECTID'] = 10.0
        gg['properties']['OBJECTID'] = 'Shell rig move'
        gg['properties']['WINDGEBIED'] = 'Shell rig move'
        y = 57.482 
        x = 2.050
        gs = 0.25
        gg['geometry']['coordinates'] = [[[x-gs,y-gs],
                                         [x-gs,y+gs],
                                         [x+gs,y+gs],
                                         [x+gs,y-gs],
                                         [x-gs,y-gs]]]
        gj['features'].append(gg)
        
        with open(gj_file, 'wb') as f:
            pickle.dump(gj, f)
    else:
        gj = pickle.load( open(gj_file,"rb") )
    
    data = {'OBJECTID':  [1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0],
            'Value': np.arange(1,11),
            }
    data = {'OBJECTID':  [g['properties']['WINDGEBIED'] for g in gj['features']],
        'Value': np.arange(1,11),
        }

    df2 = pd.DataFrame(data, columns = ['OBJECTID','Value'])    
    print(df2)
   
   
    fig = px.choropleth_mapbox(df2, geojson=gj, locations='OBJECTID', color='Value',
                               color_continuous_scale="Viridis",
                               range_color=(0, 12),
                               featureidkey='properties.WINDGEBIED',
                               #mapbox_style="stamen-watercolor",
                               #mapbox_style="carto-positron",
                               center={"lat": 55, "lon": 5},
                               zoom=5.5,
                               opacity=0.5,
                               #colorbar=None,
                               #scope="europe",
                               #labels={'unemp':'unemployment rate'}
                              )    
    fig.update_layout({'clickmode': 'event+select',
                       'mapbox_style':'open-street-map',
                       #'title':'Select a specific windpark / region, as marked on the map <Br> Zoom in or out by scrolling over the map',
                       #'title_x':0.5,
                       #'margin':go.layout.Margin(l=20,r=50,b=30,t=35,pad=4),
                       'height':1000,
                       #'width':500,
                       'autosize':True,
                       'coloraxis_showscale':False,
                       'geo':{'fitbounds':'geojson',
                              'resolution':50,
                              'showframe':False,
                              'showcountries':True}, 
                        'margin': {
                            'l': 0,
                            'r': 0,
                            'b': 0,
                            't': 0,
                            'pad': 2}
                       #'hovermode':'hovermode',
                       #'hoverinfo':'test',
                      })

    return(fig)


### Add Callbacks ###

@app3.callback(
    dash.dependencies.Output('fc_time','options'),
    dash.dependencies.Input('windparks','clickData'),
)
def update_times(clickData):
    timez_ext = pd.to_datetime(xr.open_dataset(bd+'swh_ext_2017_2020_bc.nc').time[:].values)
    dict_times = {t.strftime('%Y-%m-%d'):pd.to_datetime(t) for t in timez_ext[::-1][:100]}
    return [{'label': i,'value': i} for i in dict_times.keys()]

@app3.callback(
    dash.dependencies.Output('windparks', 'figure'),
    [dash.dependencies.Input('windparks','clickData'),
     dash.dependencies.Input('predictand', 'value'),
     dash.dependencies.Input('fc_time','value'),
    ])
def update_windpark(clickData,predictand,fc_time):
    return create_windpark(clickData,predictand,fc_time)

@app3.callback(
    dash.dependencies.Output('timeseries', 'figure'),
    dash.dependencies.Output('timeseries2', 'figure'),
    [dash.dependencies.Input('windparks','clickData'),
     dash.dependencies.Input('predictand', 'value'),
     dash.dependencies.Input('fc_time','value'),
     dash.dependencies.Input('swh_threshold','value'),
     dash.dependencies.Input('wind_threshold','value'),
     dash.dependencies.Input('chance_exceed','value'),     
     dash.dependencies.Input('sel_times','value'),
    ])
def update_timeseries(clickData,predictand,fc_time,swh_threshold,wind_threshold,chance_exceed,sel_times):
    #return dash.no_update
    return create_timeseries(clickData,predictand,fc_time,swh_threshold,wind_threshold,chance_exceed,sel_times)



if __name__ == '__main__': 
    app3.run_server(debug=True,threaded=True)#,host='0.0.0.0')#,port=80)    


        
        
