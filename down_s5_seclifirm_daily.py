#!/usr/bin/env python
from ecmwfapi import *
#from ecmwfapi import ECMWFDataServer
import datetime    
import os
import gzip
import numpy as np
import sys
import pandas as pd

"""
Author: Folmer Krikken
Contact: folmer.krikken@climateradar.com
Date: 29-10-2021

Script to download SWH and surface wind speed from ECMWF S5 wave model. Currently, it downloads the hindcast and forecasts 
ranging from 1981 to current. You need permissions from ECMWF to be able to download this data

"""


server = ECMWFService("mars")

bd = '/where/to/store/your/data/'

if not os.path.isdir(bd):
    os.system('mkdir -p '+bd)

date = datetime.date.today().isoformat()
dr = pd.date_range('1981-01-01',datetime.date.today().isoformat(),freq='MS')[::-1]

for dt in dr:
    date = dt.strftime('%Y-%m-%d')
    print(date)
    filename = 'ecmwf_s5_daily_swh_wind_'+date+'.nc'
    if os.path.isfile(bd+filename):
        print(filename+' already exists..')
        continue
    else:
        server.execute({
            'stream'    : "wasf",
            #'origin'    : "ecmf",
            'system'    : "5",
            'method'    : "1",
            'number'    : "0/to/51",
            'class'     : "od",
            'expver'    : "1",
            'date'      : date,
            'time'      : "00",
            'type'      : "fc",
            'levtype'   : "sfc",
            'param'     : "229.140/245.140",
            'step'      : "0/to/5160/by/24",
            'area'      : "61/-4/51/9",
            'format'    : "netcdf",
            'grid'      : '0.5/0.5',
            'expect'    : 'ANY'
            },
            bd+filename)
    #sys.exit()
