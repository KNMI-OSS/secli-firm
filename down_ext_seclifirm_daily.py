from ecmwfapi import *
import datetime
import os
import gzip
import numpy as np
import sys
import pandas as pd

"""
Author: Folmer Krikken
Contact: folmer.krikken@climateradar.com
Date: 29-10-2021

Script to download SWH and surface wind speed of ECMWF from the MARS server, only for the North Sea. Currently, it downloads data ranging from 2017 to current. This data is not publicly available and needs special permissions from ECMWF.

"""

server = ECMWFService("mars")

# Where to store the data
bd = '/where/to/store/your/data/'

# Step varies over leadtime, first 1-hourly, then 3-hourly and then 6-hourly
steps = '/'.join(str(i) for i in range(1,91)) + '/' + '/'.join(str(i) for i in range(93,144,3)) + '/' + '/'.join(str(i) for i in range(150,1105,6))

# Set start and end time for acquiring hindcasts and forecasts
start_date = "2017-01-01"

# For operational version, set end_date to today
end_date = datetime.date.today().isoformat()

# Construct date range for all mondays and thursday
d1 = pd.date_range(start_date, end_date, freq="W-MON")
d2 = pd.date_range(start_date, end_date, freq="W-THU")
datez = d1.union(d2)

# For operational version, update daily
#datez = pd.date_range(start_date,end_date, freq='D')

# Loop over all dates, but start with the latest / most recent date
for dat in datez[::-1]:
    
    ## Loop over deterministic, control and perturbed forecasts to get all three
    #for stream,fc_type in zip(['wave','waef','waef'],['fc','cf','pf']):
    for stream,fc_type in zip(['waef','waef'],['cf','pf']):
        
        filename = 'ecmwf_extfc_swh_wind_'+fc_type+'_'+dat.strftime('%Y-%m-%d')+'.nc' 
        if os.path.isfile(bd+filename):
            print('ensemble already there..')
        else:
            server.execute({
                'stream'    : stream,
                'type'      : fc_type,
                'class'     : "od",
                'expver'    : "0001",
                'date'      : dat.strftime('%Y-%m-%d'),
                'time'      : "00",
                'levtype'   : "sfc",
                'number'    : "1/to/50",
                'param'     : "229.140/245.140",        # SWH / Windspeed
                'step'      : steps,
                'area'      : "51/-4/61/10",
                'grid'      : ".5/.5",
                'format'    : "netcdf",
                'expect'    : "ANY",
                },
                bd+filename)            
    